create table item (
    id INT AUTO_INCREMENT PRIMARY KEY,
    description varchar(100),
    available boolean,
    price decimal
);

insert into item (id, description, price, available) values (1, 'item 1', 1.45, true);
insert into item (id, description, price, available) values (2, 'item 2', 1.25, true);
insert into item (id, description, price, available) values (3, 'item 3', 1.30, false);
insert into item (id, description, price, available) values (4, 'item 4', 1.30, true);
insert into item (id, description, price, available) values (5, 'item 5', 1.30, true);
insert into item (id, description, price, available) values (6, 'item 6', 1.30, false);
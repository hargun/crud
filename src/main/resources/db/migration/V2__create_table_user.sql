create table user (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name varchar(100),
    email varchar(100),
    password varchar(100),
    role varchar(20)
);

insert into user (id, name, email, password, role) values (1, 'hargun', 'h@me.com', '$2y$12$gVimCoz4Tfx21YZsof1FgOgjPaLN8uXDsXAfmiphJ6C4XJA0mj5gq', 'admin');
insert into user (id, name, email, password, role) values (2, 'bob', 'b@me.com', '$2y$12$gVimCoz4Tfx21YZsof1FgOgjPaLN8uXDsXAfmiphJ6C4XJA0mj5gq', 'guest');
insert into user (id, name, email, password, role) values (3, 'casper','c@me.com', '$2y$12$gVimCoz4Tfx21YZsof1FgOgjPaLN8uXDsXAfmiphJ6C4XJA0mj5gq', 'guest');
package com.hrs.crud.Services;

import com.hrs.crud.Entities.Item;
import com.hrs.crud.Repositories.ItemRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ItemServiceImpl implements ItemService {
    private ItemRepository itemRepository;

    public List<Item> getLife() {
        return itemRepository.findAll();
    }
}
package com.hrs.crud.Services;

import com.hrs.crud.Entities.User;

public interface UserService {
    User findByEmail(String email);
}

package com.hrs.crud.controllers;

import com.hrs.crud.Entities.Item;
import com.hrs.crud.Services.ItemService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class ItemController {
    private ItemService itemService;

    @GetMapping("/")
    public List<Item> getIndex() {
        return itemService.getLife();
    }
}
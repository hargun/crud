package com.hrs.crud.Services;

import com.hrs.crud.Repositories.ItemRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ItemServiceImplTest {
    @Mock
    private ItemRepository mockItemRepository;

    @InjectMocks
    private ItemServiceImpl subject;

    @Test
    public void getLife() {
        subject.getLife();

        verify(mockItemRepository).findAll();
    }
}